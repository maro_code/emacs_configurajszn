(add-to-list 'load-path "~/elisp/php-mode")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(require 'php-mode)

(setq inhibit-startup-message t)
;;(setq inhibit-startup-screen t)

(global-linum-mode 1)
(global-hl-line-mode 1)
(setq-default indent-tabs-mode nil)

(defun insert-php-tag ()
  "Insert PHP markup <?php  ?>"
  (interactive)
  (insert "<?php  ?>")
  (backward-char 3))

(defun insert-php-print-global-tag ()
  "Insert PHP for printing global array item"
  (interactive)
  (insert "<?php g(\"\"); ?>")
  (backward-char 6))

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)

(defun copy-line()
  "copy line to clipboard"
  (interactive)
  (move-beginning-of-line nil)
  (set-mark-command nil)
  (move-end-of-line nil)
  (kill-ring-save (region-beginning) (region-end))
  (move-beginning-of-line nil)
)

(defun copy-whole-buffer ()
  (interactive)
  (save-excursion
    (mark-whole-buffer)
    (copy-region-as-kill 1 (buffer-size))))

;;(defun paste-a-buffer ()
;;  (interactive)
;;  (insert-buffer "name")
;;  )

(defun compile-in-dir (dir command)
  (interactive "DCompile in directory: \nsCommand: ")
  (let ((default-directory dir))
    (compile command)))

(defun goto-match-paren (arg)
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

(global-set-key (kbd "<f2>") 'copy-line)
(global-set-key (kbd "<f4>") 'previous-buffer)
(global-set-key (kbd "<f5>") 'next-buffer)
(global-set-key (kbd "<f6>") 'ibuffer)
(global-set-key (kbd "<f7>") 'other-window)
(global-set-key (kbd "<f8>") 'compile-in-dir)
(global-set-key (kbd "<f9>") 'copy-whole-buffer)

(global-set-key (kbd "C-x t") 'duplicate-line)
(global-set-key (kbd "C-%") 'goto-match-paren)

(global-set-key (kbd "C-x p") 'insert-php-tag)
(global-set-key (kbd "C-x g") 'insert-php-print-global-tag)

(add-hook 'python-mode-hook '(lambda () 
  (setq python-indent 2)
  (electric-indent-mode)
))


;;keep cursor at same position when scrolling
(setq scroll-preserve-screen-position 1)
;;scroll window up/down by one line
(global-set-key (kbd "M-n") (kbd "C-u 1 C-v"))
(global-set-key (kbd "M-p") (kbd "C-u 1 M-v"))

(setq w32-use-visible-system-caret nil)
(setq-default cursor-type 'block)
(load-theme 'deeper-blue)

(put 'erase-buffer 'disabled nil)

;;(find-file "/...")
