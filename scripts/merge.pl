#!/usr/bin/perl

die "Usage: <dictionary> <input>\n" if ((scalar @ARGV) < 2);

my $current = "@ARGV[0]";
my $neww = "@ARGV[1]";

open CURRENT_FILE, "$current" or die "Unable to open '$current' file\n";
my @current_lines = <CURRENT_FILE>;
close CURRENT_FILE;
chomp @current_lines;

open NEWW_FILE, "$neww" or die "Unable to open '$neww' file\n";
my @neww_lines = <NEWW_FILE>;
close NEWW_FILE;
chomp @neww_lines;

my @sum_array;

foreach (@current_lines) {
    parseline($_);
}

foreach (@neww_lines) {
    parseline($_);
}

@unique_list = uniq(@sum_array);
@final_list = sort @unique_list;

open(my $file_handle, '>', $current) or die "Unable to open '$current' $! \n";

foreach(@final_list) {
    print $file_handle $_ . "\n";
}

close $file_handle;

########################################################

sub parseline {
    @items_list = split /\s+/, $_;
    foreach (@items_list) {
        chomp;
        if (/^[^\w]/) { next; }
        if (/^[\d]/) { next; }
        s/[^\w]+$//g;
        if ( length ($_) < 6 ) { next; }
        push @sum_array, lc $_;
    }
}

sub uniq {
    my %seen;
    return grep { !$seen{$_}++ } @_;
}
